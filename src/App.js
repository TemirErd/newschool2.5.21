import React, {Component, Fragment} from 'react'
//import OnImagesLoaded from 'react-on-images-loaded'
import Loader from './components/UI/Loader/Loader'

import images from './components/Images/Images'

import 'normalize-css'
import './scss/preset.scss'
import './scss/App.scss';
import './scss/media.scss';

import Slider from './components/Slider/Slider'
import GalleryImgs from './components/Gallery/GalleryImgs'

import VanillaScrollspy from 'vanillajs-scrollspy';

class App extends Component {
  //DATA=========================================
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      windowState: {
        width: 0,
        height: 0
      },
      currentYear: null,
      baseUrl: '/landing/'

    }
  }

//METHODS=========================================
  menuScroll() {/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
      let currentScrollPos = window.pageYOffset;
      const navbar = document.getElementById("navbar")
      if (prevScrollpos > currentScrollPos) {
        navbar.style.top = "0"
        if (window.scrollY > 0) {
          navbar.classList.add('active')
          navbar.classList.add('scroll')
        } else {
          navbar.classList.remove('active')
          navbar.classList.remove('scroll')

          const menu = document.querySelector('#navbar .menu')
          menu.classList.remove('active')
        }
      } else {
        if (window.scrollY > 100) {
          navbar.style.top = "-80px"
        }
        navbar.classList.remove('active')

        const menu = document.querySelector('#navbar .menu')
        menu.classList.remove('active')
      }

      prevScrollpos = currentScrollPos;
    }
  }

  menuBurger() {
    const menu = document.querySelector('#navbar .menu')
    menu.classList.toggle('active')
  }

  menuBurgerHide() {
    const menu = document.querySelector('#navbar .menu')
    menu.classList.remove('active')
  }

  setCurrentYear() {
    let d = new Date();
    let n = d.getFullYear();

    this.setState({currentYear: n})
  }

  imagesLoading() {
    return images.map((url, idx) => (
      <img src={`${this.state.baseUrl}${url}`} key={idx} alt={'imgLoad'}/>
    ))
  }

  imagesWithBaseUrl(imgId){
    return `${this.state.baseUrl}${images[imgId]}`
  }

  onLoadedHandler() {
    this.setState({loading: false})

    this.navbarInit()
  }

  handleTimeout() {
    //this.props.showError()
    alert("hit timeout. this is the onTimeout function being run and is mounting normally now")
    this.setState({loading: false})
  }


  handleResize = () => {
    this.setState(prevState => ({
      windowState: {                   // object that we want to update
        ...prevState.windowState,    // keep all other key-value pairs
        width: window.innerWidth,       // update the value of specific key
        height: window.innerHeight,       // update the value of specific key
      }
    }))
  }

  navbarInit(){
    let navbar = document.querySelector('#navbar');
    if (navbar !== null) {
      const scrollspy = new VanillaScrollspy(navbar);
      scrollspy.init();
    } else {
      setTimeout(() => {
        navbar = document.querySelector('#navbar');
        const scrollspy = new VanillaScrollspy(navbar);
        scrollspy.init();
      }, 1000)
    }
  }

  componentDidMount() {
setTimeout(()=>{
  this.setState({loading: false})

  this.navbarInit()
}, 500)
    //получить размер экрана
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    this.setCurrentYear()

    this.menuScroll()

  }

  //VIEW=========================================
  render() {
    let bgStyle = {
      backgroundImage: `url(${this.imagesWithBaseUrl(11)})`
    }
    if (this.state.windowState.width > 760) {
    }
    return (
      <Fragment>
        {
          this.state.loading
            ? <Loader/>
            : null
        }

          <div className="imagesLoading">
            {this.imagesLoading()}
          </div>
          {
            !this.state.loading
              ? <div id="contentInner">
                <section id="navbar">
                  <div className="container flex">
                    <img src={this.imagesWithBaseUrl(0)} className="logo" alt="logo"/>

                    {
                      this.state.windowState.width < 760 ?
                        <div className="flex flexMob">
                          <div className="burger" onClick={this.menuBurger}>
                            <div></div>
                            <div></div>
                            <div></div>
                          </div>
                          <a href="tel:87273560007" className="call"><span>Позвонить</span></a>
                        </div>
                        : null
                    }

                    <div className={'menu flex'}>
                      <a onClick={this.menuBurgerHide} href="#section1">О нас</a>
                      <a onClick={this.menuBurgerHide} href="#section2">Наши ученики</a>
                      <a onClick={this.menuBurgerHide} href="#section4">Галерея</a>
                      <a onClick={this.menuBurgerHide} href="#section5">Обращение директора</a>
                      <a onClick={this.menuBurgerHide} href="https://newschool.kz/news/" target={'_blank'}
                         rel="noreferrer">Новости</a>
                      <a onClick={this.menuBurgerHide} href="#section6">Контакты</a>
                    </div>


                  </div>
                </section>
                {/*.menu*/}
                <section id="top">
                  <div className="container">
                    <div className="flex flex1">
                      <div className="item item1">
                        <img src={this.imagesWithBaseUrl(2)} className="img2 el el1" alt="img2"/>
                        <img src={this.imagesWithBaseUrl(3)} className="img3 el el2" alt="img3"/>
                        <img src={this.imagesWithBaseUrl(4)} className="img4 el el3" alt="img4"/>

                        <div className="wrap flex flex2">
                          <h1>Новая школа <span>- это больше чем Школа</span></h1>
                          <div className="sub">Качественное образование - залог счастливого детства и успешного будущего.
                          </div>

                          <div className="btn" onClick={() => {
                            document.querySelector('#section3').scrollIntoView({block: "start", behavior: "smooth"})
                          }
                          }>записаться на визит
                          </div>
                        </div>
                      </div>
                      <div className="item item2">
                        <img src={this.imagesWithBaseUrl(1)} className="img1" alt="img1"/>
                        <div className="circle"></div>
                      </div>

                    </div>

                  </div>
                </section>
                {/*.top*/}
                <section id="section1">
                  <div className="container">
                    <h2>Почему родители выбирают нас</h2>
                    <div className="flex">
                      <div className="item item1">
                        <div className="bg" style={bgStyle}></div>

                        <img src={this.imagesWithBaseUrl(5)} className="img5 imgS1" alt="img4"/>
                        <img src={this.imagesWithBaseUrl(6)} className="img6 imgS2" alt="img4"/>

                        <div className="bottom">
                          <div className="title">Продвинутые технологии</div>
                          <div className="txt">Наши кабинеты оснащены Lego, Arduino, компьютерами, необходимым программным
                            обеспечением.
                          </div>
                        </div>
                      </div>
                      {/*.item*/}
                      <div className="item item2">
                        <div className="bg" style={bgStyle}></div>

                        <img src={this.imagesWithBaseUrl(7)} className="img7 imgS1" alt="img4"/>
                        <img src={this.imagesWithBaseUrl(8)} className="img8 imgS2" alt="img4"/>

                        <div className="bottom">
                          <div className="title">Развлечения и кружки</div>
                          <div className="txt">Шахматный клуб, вокал, художество, музыкальный клуб, робототехника, таеквандо и много другое не дадут скучать вашему ребенку</div>
                        </div>
                      </div>
                      {/*.item*/}
                      <div className="item item3">
                        <div className="bg" style={bgStyle}></div>

                        <img src={this.imagesWithBaseUrl(9)} className="img9 imgS1" alt="img4"/>
                        <img src={this.imagesWithBaseUrl(10)} className="img10 imgS2" alt="img4"/>

                        <div className="bottom">
                          <div className="title">Высокий рейтинг</div>
                          <div className="txt">“Наша школа занимает лидирующие позиции в стране. А ученики становятся успешными обладателями премий и наград”</div>
                        </div>
                      </div>
                      {/*.item*/}
                    </div>
                  </div>
                </section>
                {/*.section1*/}
                <section id="section2">
                  <div className="container">
                    <h2>Наши ученики - победители конкурсов и олимпиад</h2>
                    <div className="sub">Результативность обучения обеспечивает активное включение детей в исследовательскую проектную деятельность, позволяющая им проявлять ответственность и самостоятельность</div>

                    <Slider
                      windowSize={this.state.windowState}
                      slides={
                        [
                          {
                            img: this.imagesWithBaseUrl(12),
                            name: 'Янушко Дарья',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2015 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(13),
                            name: 'Ешенкуловая Дамель',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2015 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(14),
                            name: 'Лаврова Арина',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2016 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(15),
                            name: 'Зайцева Екатерина',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2015 г.',
                          },

                          {
                            img: this.imagesWithBaseUrl(24),
                            name: 'Иоффе Элина',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2016 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(25),
                            name: 'Зинченко Полина',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2019 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(26),
                            name: 'Зафар Алина',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2019 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(27),
                            name: 'Филимонова Анна',
                            txt: 'Награждена знаком ``Алтын белгi`` - 2019 г.',
                          },
                          {
                            img: this.imagesWithBaseUrl(28),
                            name: 'Банникова Валерия',
                            txt: 'Победитель международного конкурса научных проектов 2016 г.',
                          },
                        ]
                      }
                    />

                    <div className="flex flex2">
                      <div className="item">
                        <div className="title">
                          Более 50
                        </div>
                        <div className="txt">Призеров международных и республиканских творческих конкурсов</div>
                      </div>
                      {/*.item*/}
                      <div className="item">
                        <div className="title">15</div>
                        <div className="txt">Знаков “Алтын Белгі”</div>
                      </div>
                      {/*.item*/}
                      <div className="item">
                        <div className="title">Более 40</div>
                        <div className="txt">Наград в сфере Информационных Технологий</div>
                      </div>
                      {/*.item*/}
                    </div>
                  </div>
                </section>
                {/*.section2*/}
                <section id="section3">
                  <div className="container">
                    <h2>Хотите увидеть все своими глазами?</h2>
                    <div className="sub">Прежде, чем записать ребенка в школу, можете ознакомиться со школой, её
                      условиями, и атмосферой.
                    </div>
                    <div className="flex">
                      <div className="item item1">
                        <img src={this.imagesWithBaseUrl(16)} className={'img'} alt="img"/>

                        <img src={this.imagesWithBaseUrl(17)} className={'el el1'} alt="img"/>
                        <img src={this.imagesWithBaseUrl(18)} className={'el el2'} alt="img"/>
                        <img src={this.imagesWithBaseUrl(19)} className={'el el3'} alt="img"/>
                      </div>
                      <div className="item item2">
                        <img src={this.imagesWithBaseUrl(20)} className={'el el4'} alt="img"/>
                        <div id="form">
                          <div className="title">Прежде чем записать ребенка приглашаем ознакомится со школой ее условиями и атмосферой</div>
                          <form>
                            <input className="input" placeholder='Имя ребенка' type="text"/>
                            <input className="input" placeholder='Имя родителя' type="text"/>
                            <input className="input" placeholder='Телефон' type="text"/>
                            <input type="button" className="btn blue" value={'записаться на визит'}/>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                {/*.section3*/}
                <section id="section4">
                  <div className="container">
                    <h2>Фото галерея</h2>

                    {
                      this.state.windowState.width < 760
                        ?
                        <Slider
                          windowSize={this.state.windowState}
                          slides={
                            [
                              {
                                img: this.imagesWithBaseUrl(22),
                              },
                              {
                                img: this.imagesWithBaseUrl(30),
                              },
                              {
                                img: this.imagesWithBaseUrl(32),
                              },
                              {
                                img: this.imagesWithBaseUrl(34),
                              },
                              {
                                img: this.imagesWithBaseUrl(36),
                              },
                              {
                                img: this.imagesWithBaseUrl(38),
                              },
                              {
                                img: this.imagesWithBaseUrl(40),
                              },
                            ]
                          }
                        />
                        :
                        <GalleryImgs
                          imgs={
                            [
                              {
                                imgOriginal: this.imagesWithBaseUrl(22),
                                imgThumbnail: this.imagesWithBaseUrl(21)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(30),
                                imgThumbnail: this.imagesWithBaseUrl(29)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(32),
                                imgThumbnail: this.imagesWithBaseUrl(31)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(34),
                                imgThumbnail: this.imagesWithBaseUrl(33)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(36),
                                imgThumbnail: this.imagesWithBaseUrl(35)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(38),
                                imgThumbnail: this.imagesWithBaseUrl(37)
                              },
                              {
                                imgOriginal: this.imagesWithBaseUrl(40),
                                imgThumbnail: this.imagesWithBaseUrl(39)
                              },

                            ]
                          }
                        />
                    }


                  </div>
                </section>
                {/*.section4*/}
                <section id="section5">
                  <div className="container">
                    <h3>Обращение директора</h3>
                    <div className="flex">
                      <div className="item item1">
                        <img src={this.imagesWithBaseUrl(41)} alt="img" className="el el1"/>
                        <img src={this.imagesWithBaseUrl(42)} alt="img" className="el el2"/>
                        <div className="name">Гульнара Маутканова Карамендинова</div>
                        <div className="prof">Директор школы</div>
                        {
                          this.state.windowState.width < 760
                            ?
                              <img src={this.imagesWithBaseUrl(23)} alt="img"/>
                            : null
                        }
                        <div className="txt">
                          <p>Дорогие родители!</p>
                          <p>На протяжении 28 лет наша школа обучает творческих и талантливых ребят. Время идет, и мы не
                            стоим на месте. Информационные технологии проникают во все сферы жизни, и наша задача на
                            сегодня — привить нашим детям новые навыки.</p>
                          <p>Новая школа — это место, в котором дети не только получают качественное образование, но и
                            строят крепкие и дружеские отношения.</p>
                          <p>Новая школа — это территория творчества. Мы искренне хотим, чтобы наши дети состоялись, нашли
                            свою нишу в жизни через развитие креативных способностей.</p>
                          <p>В нашей школе мы также учим каждого ребенка не сравнивать себя с другими, а наблюдать
                            исключительно за собой. Ведь так важно отслеживать личный рост, формировать внутреннюю опору и
                            веру в себя.</p>
                          <p>Новая школа - это школа, в которой дети хотят учиться!</p>
                        </div>

                        <div className="btn" onClick={() => {
                          document.querySelector('#section3').scrollIntoView({block: "start", behavior: "smooth"})
                        }
                        }>Записаться на визит
                        </div>
                      </div>
                      {
                        this.state.windowState.width > 760
                          ?
                          <div className="item item2">
                            <img src={this.imagesWithBaseUrl(23)} alt="img"/>
                          </div>
                          : null
                      }

                    </div>
                  </div>
                </section>
                {/*.section5*/}
                <section id="section6">
                  <div className="container">
                    <h3>Как добраться и связаться с нами</h3>
                    <div className="flex flex1">
                      <div className="item item1">
                        <div className="map">
                          <iframe
                            title="map"
                            src="https://yandex.ru/map-widget/v1/?um=constructor%3A3a903407cbb7a7b791abe519203755c71824722ee0b989224cfda1151b99c446&amp;source=constructor&amp;scroll=false"
                            width="100%" height="470" frameBorder="0"></iframe>
                        </div>
                        {/*.map*/}
                      </div>
                      <div className="item item2">
                        <div className="contacts flex flex2">
                          <div className="item">
                            <div className="title">Телефон</div>
                            <ul>
                              <li>Директор – <a href="tel:3560007">356 00 07</a> (вн. 702)</li>
                              <li>Завуч - <a href="tel:3560007">356 00 07</a> (вн. 701, 703, 704, 705)</li>
                              <li>Бухгалтерия - <a href="tel:3560007">356 00 07</a> (вн. 711)</li>
                              <li>Завхоз - <a href="tel:3560007">356 00 07</a> (вн. 710)</li>
                            </ul>
                          </div>
                          {/*.item*/}
                          <div className="item">
                            <div className="title">Мы находимся тут</div>
                            <div className="txt">Республика Казахстан, г. Алматы, ул. Утеген батыра, 102а</div>
                          </div>
                          {/*.item*/}
                          <div className="item">
                            <div className="title">Напишите нам</div>
                            <a href="mailto:info@newschool.kz">info@newschool.kz</a>
                          </div>
                          {/*.item*/}
                          <div className="item">
                            <div className="title">Мы в соцсетях</div>
                            <div className="soc flex">
                              <a href="https://www.youtube.com/channel/UC51pTq5T7Ll8ue3ZB4Vx6uw/featured" target="_blank"
                                 rel="noreferrer"><img
                                src={this.imagesWithBaseUrl(43)} alt="img"/></a>
                              <a href="https://www.instagram.com/newschool_kz/?hl=ru" target="_blank"
                                 rel="noreferrer"><img
                                src={this.imagesWithBaseUrl(44)} alt="img"/></a>
                            </div>
                          </div>
                          {/*.item*/}

                        </div>
                        {/*.contacts*/}
                      </div>
                    </div>
                  </div>
                </section>
                {/*.section6*/}
                <section id="footer">
                  <div className="container">
                    Copyright @ {this.state.currentYear}, Жансерикова Аяла, Все права защищены
                  </div>
                </section>
                {/*.footer*/}
              </div>
              : null
          }

      </Fragment>
    );
  }
}

export default App;
