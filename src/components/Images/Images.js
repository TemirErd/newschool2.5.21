let prefixUrl = ''

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  prefixUrl = '../../..'
}

const images = [
  prefixUrl + '/imgs/logo.png',
  prefixUrl + '/imgs/img1.png',
  prefixUrl + '/imgs/img2.png',
  prefixUrl + '/imgs/img3.png',
  prefixUrl + '/imgs/img4.png',
  prefixUrl + '/imgs/img5.png',
  prefixUrl + '/imgs/img6.png',
  prefixUrl + '/imgs/img7.png',
  prefixUrl + '/imgs/img8.png',
  prefixUrl + '/imgs/img9.png',
  prefixUrl + '/imgs/img10.png',
  prefixUrl + '/imgs/img11.png',

  prefixUrl + '/imgs/img12.png',
  prefixUrl + '/imgs/img13.png',
  prefixUrl + '/imgs/img14.png',
  prefixUrl + '/imgs/img15.png',

  prefixUrl + '/imgs/img16.png',
  prefixUrl + '/imgs/img17.png',
  prefixUrl + '/imgs/img18.png',
  prefixUrl + '/imgs/img19.png',
  prefixUrl + '/imgs/img20.png',

  prefixUrl + '/imgs/img21.png',
  prefixUrl + '/imgs/img22.jpg',

  prefixUrl + '/imgs/img23.png',

  prefixUrl + '/imgs/img24.jpg',
  prefixUrl + '/imgs/img25.jpg',
  prefixUrl + '/imgs/img26.jpg',
  prefixUrl + '/imgs/img27.jpg',
  prefixUrl + '/imgs/img28.jpg',

  prefixUrl + '/imgs/img29.png',
  prefixUrl + '/imgs/img30.jpg',
  prefixUrl + '/imgs/img31.png',
  prefixUrl + '/imgs/img32.jpg',
  prefixUrl + '/imgs/img33.jpg',
  prefixUrl + '/imgs/img34.jpg',
  prefixUrl + '/imgs/img35.png',
  prefixUrl + '/imgs/img36.jpg',
  prefixUrl + '/imgs/img37.png',
  prefixUrl + '/imgs/img38.jpg',
  prefixUrl + '/imgs/img39.png',
  prefixUrl + '/imgs/img40.jpg',

  prefixUrl + '/imgs/img41.png',
  prefixUrl + '/imgs/img42.png',

  prefixUrl + '/imgs/img43.png',
  prefixUrl + '/imgs/img44.png',

  prefixUrl + '/imgs/img45.svg',

]

export default images