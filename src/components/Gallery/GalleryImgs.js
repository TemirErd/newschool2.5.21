import React from 'react'
import 'photoswipe/dist/photoswipe.css'
import 'photoswipe/dist/default-skin/default-skin.css'

import {Gallery, Item} from 'react-photoswipe-gallery'

const GalleryImgs = props => {
  return (
    <div className="GalleryImgs">
      <Gallery shareButton={false} fullscreenButton={false}>
        { props.imgs.map((img, index) => {
          return (
            <Item key={index}
              original={img.imgOriginal}
              thumbnail={img.imgThumbnail}
              width="1024"
              height="576"
            >
              {
                ({ref, open}) => (
                <img className="img" ref={ref} onClick={open} alt="img" src={img.imgThumbnail}/>

                )
              }
            </Item>
          )
        }) }

      </Gallery>
    </div>
  )
}

export default GalleryImgs