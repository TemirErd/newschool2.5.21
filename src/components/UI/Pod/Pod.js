import react from 'react'
import classes from './Pod.module.scss'
import images from "../../Images/Images";

const Pod = props => {
  return (
    <section className={classes.Pod}>
        <div className="container">
          <div className={classes.title}>Материал подготовлен при поддержке</div>
          <a href="https://www.samsung.com/kz_ru/" target={'_blank'} rel="noreferrer"><img
            src={images[0]}
            alt="logo"/></a>
        </div>
    </section>
  )
}

export default Pod